import React, { Component } from "react";
import ListState from './Components/ListState';

class App extends Component {
  render() {
    return (
      <section className="content">
        <ListState />
      </section>
    )
  }
}

export default App;
