import React, { Component } from "react";
import Search from './Search';
import FetchWeather from '../../Api/index';
import ListaCidades from './ListaCidades';
import './index.css';

class ListState extends Component {
    constructor(props){
        super(props);
        this.state ={
            cidades:[],
            cidadeAtiva: {}
        }
        this.changeBox = this.changeBox.bind(this);
    }

    componentDidMount(){
        var listState = ["riodejaneiro", "sao paulo", "belo horizonte", "brasilia", "belem", "salvador", "curitiba", "fortaleza", "manaus", "joao pessoa"];
        const cidades = listState.map(function(item){
            return FetchWeather(item).then(function (response) {
                return response.data.query
            })
        });

        const me = this
        Promise.all(cidades)
        .then(function(values) {
           me.setState({cidades: values});
        });
    }

    changeBox(item){
        this.setState({cidadeAtiva: item});
        if (window.matchMedia("(max-width: 480px)").matches) {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    }

  render() {
    return (
        <div className={`content__box-weather `}>
          <header className="content__box-weather__header">
            <h1>Previsão do tempo</h1>
            <Search cidadeAtiva={this.state.cidadeAtiva} changeBox={this.changeBox} />
          </header>

          <div className="content__box-weather__box-state">
            <h2>Capitais</h2>
            <div className="min-max">
                <span>Min</span>
                <span>Max</span>
            </div>
            <div className="min-max mobile">
                <span>Min</span>
                <span>Max</span>
            </div>
            <div className="content__box-weather__box-state__list" >
                <ListaCidades ArrayLista={this.state.cidades} changeBox={this.changeBox} />
            </div>
          </div>
        </div>
    )
  }
}

export default ListState;
