import React, { Component } from "react";
import FetchWeather from '../../Api/index';
import './index.css';

import IcoClose  from './images/ico-close.svg';

function temperatureConverter(valNum) {
    valNum = parseFloat(valNum);
    return parseInt((valNum-32)/1.8);
}

function mostraDiaSemana(item){
    switch(item) {
        case 'Sun':
            item = 'Dom'
            break;
        case 'Mon':
            item = 'Seg'
            break;
        case 'Tue':
            item = 'Ter'
            break;
        case 'Wed':
            item = 'Qua'
            break;
        case 'Thu':
            item = 'Qui'
            break;
        case 'Fri':
            item = 'Sex'
            break;
        case 'Sat':
            item = 'Sáb'
            break;
        default:
            break;
    }
    return item;
}

function statusCondicao(item){
    switch(item) {
        case 'Thunderstorms':
            item = 'Trovoadas'
            break;
        case 'Mostly Clear':
            item = 'Claro'
            break;
        case 'clear':
            item = 'Limpo'
            break;
        case 'Scattered Thunderstorms':
            item = 'Tempestades'
            break;
        case 'Breezy':
            item = 'Brisa'
            break;
        case 'Partly Cloudy':
            item = 'Parcialmente Nublado'
            break;
        case 'Cloudy':
            item = 'Nublado'
            break;
        case 'Mostly Cloudy':
            item = 'Muito Nublado'
            break;
        case 'Mostly Sunny':
            item = 'Ensolarado'
            break;
        case 'Sunny':
            item = 'Sol'
            break;
        default:
            break;
    }
    return item;
}

class Search extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: '',
            ErrorCity: false,
            cidadeAtiva: {}
        }
        this.handleSearch = this.handleSearch.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.closeForecast = this.closeForecast.bind(this);
    }

    closeForecast() {
        this.setState({cidadeAtiva: {}})
    }

    handleChange(event){
        this.setState({value: event.target.value});
    }

    handleSearch(event){
        event.preventDefault();
        if(this.state.value !== ''){
            let me = this;
            
            FetchWeather(this.state.value).then(function (response) {
                var cidade = response.data.query;
                if(cidade.results.channel.location == undefined) {
                    me.setState({ErrorCity: true});
                }else{
                    me.setState({cidadeAtiva: cidade});
                    me.setState({ErrorCity: false});
                    document.querySelector('#search').value = "";
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }else {
            console.log("Está vazio")
        }
    }

    renderForecast(item, index){
        return (
            <li key={index} className="content__box-weather__box-forecast__box-forecast__list-forecast__forecast">
                <div>
                    <strong>{mostraDiaSemana(item.day)}</strong>
                    <span>{temperatureConverter(item.low)}&deg;</span>
                    <span>{temperatureConverter(item.high)}&deg;</span>
                </div>
            </li>
        )
    }

    componentDidUpdate(prevProps, prevState, snapshot){
        if (this.props.cidadeAtiva !== prevProps.cidadeAtiva) {
            this.setState({cidadeAtiva: this.props.cidadeAtiva})
        }
    }

    render(){
        return(
            <div>
                {this.state.cidadeAtiva.results &&
                <div className="content__box-weather__box-forecast">
                    <h3>{`${this.state.cidadeAtiva.results.channel.location.city}, ${this.state.cidadeAtiva.results.channel.location.region} - ${this.state.cidadeAtiva.results.channel.location.country}`}</h3>
                    <a onClick={this.closeForecast}><img src={IcoClose} alt="Fechar" title="Fechar" /></a>
                    <h2>{`${temperatureConverter(this.state.cidadeAtiva.results.channel.item.condition.temp)}`}&deg;C {`${statusCondicao(this.state.cidadeAtiva.results.channel.item.condition.text)}`}</h2>
                    <ul className="content__box-weather__box-forecast__list-conditions">
                        <li className="content__box-weather__box-forecast__list-conditions__condition">
                            <span className="min"><strong>{this.state.cidadeAtiva.results.channel.item.forecast[0].low}&deg;</strong></span>
                            <span className="max"><strong>{this.state.cidadeAtiva.results.channel.item.forecast[0].high}&deg;</strong></span>
                        </li>

                        <li className="content__box-weather__box-forecast__list-conditions__condition">
                            <span className="wind">Vento <strong>{this.state.cidadeAtiva.results.channel.wind.speed}km/h</strong></span>
                        </li>

                        <li className="content__box-weather__box-forecast__list-conditions__condition">
                            <span className="wind">Sensação <strong>{this.state.cidadeAtiva.results.channel.item.condition.temp}&deg;C</strong></span>
                        </li>

                        <li className="content__box-weather__box-forecast__list-conditions__condition">
                            <span className="wind">Umidade <strong>{this.state.cidadeAtiva.results.channel.atmosphere.humidity}%</strong></span>
                        </li>
                    </ul>

                    <div className="content__box-weather__box-forecast__box-forecast">
                        <ul className="content__box-weather__box-forecast__box-forecast__list-forecast">
                            {this.state.cidadeAtiva.results.channel.item.forecast.slice(1,6).map(this.renderForecast)}
                        </ul>
                    </div>
                </div>
                }
                <div className="content__box-weather__header__box-search">
                    <input type="text" placeholder="Insira aqui o nome da cidade" id="search" onChange={this.handleChange} />
                    {this.state.ErrorCity &&
                        <span className="error-city">A cidade está errada ou não existe. Tente novamente!</span>
                    }
                    <button className="btn-search" onClick={this.handleSearch} ></button>
                </div>
            </div>
        )
    }
}

export default Search;