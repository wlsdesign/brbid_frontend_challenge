import React, { Component } from "react";
import './index.css';



class ListaCidades extends Component {

    constructor(props){
        super(props);
        this.renderItem = this.renderItem.bind(this);
    }
    renderItem(item, index){
        let NomeCidade = item.results.channel.location.city;
        let Min = item.results.channel.item.forecast[0].low;
        let Max = item.results.channel.item.forecast[0].high;
        return (
            <li key={index} className={`content__box-weather__box-state__list__list-state--list-item`}>
                <a onClick={() => this.props.changeBox(item)}>
                    <span>{Min}&deg;</span> <span>{Max}&deg;</span> {NomeCidade}
                </a>
            </li>
        )
    }

    render(){
        
        return(
            <ul className="content__box-weather__box-state__list__list-state">
                {this.props.ArrayLista.map(this.renderItem)}
            </ul>
        )
    }
}

export default ListaCidades;

